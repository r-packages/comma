#' Create a contributors table from a comma matrix map specification
#'
#' @param matrixMap The `comma` contributor Matrix Map specification
#' @param output Either `NULL` to not write anything, or the path to a
#' spreadsheet to write to.
#' @param contributors Optionally, a vector (or in the future, list) of
#' contributors to prefill the contributors table with.
#' @param nContrib Number of rows for contributors to include
#' @param type The type of contributor table to write; currently, only a
#' simple type is supported.
#' @param embed_matrixMap Whether to embed the matrix map itself.
#'
#' @return The contributors table as data frame
#' @export
#'
#' @examples ### Add something
contributorsTable_from_matrixMap <- function(matrixMap,
                                             output = NULL,
                                             contributors = c("Example name 1",
                                                              "Example name 2"),
                                             nContrib = 10,
                                             type = "simple",
                                             embed_matrixMap = TRUE) {

  matrixMap_sheetNames <- comma::opts$get('matrixMap_sheetNames');
  contributorTable_sheetNames <- comma::opts$get('contributorTable_sheetNames');
  matrixMap_colNames <- comma::opts$get('matrixMap_colNames');
  itemLabelCol <- matrixMap_colNames$items_specific$item_specific_label;
  itemIdCol <- matrixMap_colNames$items_specific$item_specific_id;
  itemDescrCol <- matrixMap_colNames$items_specific$item_specific_description;

  contributorColName <- comma::opts$get('contributorTable_contributorColName');

  if (is.character(matrixMap) && exists(paste0("comma_", matrixMap))) {
    matrixMap <- get(paste0("comma_", matrixMap));
  }

  if (!inherits(matrixMap, "comma_matrixMap")) {
    stop("As argument `matrixMap`, you have to pass a {comma} matrix map ",
         "specification as imported with ",
         "`comma::matrixMap_fromSpreadsheet()`. However, you passed an object ",
         "of class ", vecTxtQ(class(matrixMap)), ".");
  }

  if (any(is.na(matrixMap[[matrixMap_sheetNames$items_specific]][, itemLabelCol]))) {
    labelLess <-
      which(is.na(matrixMap[[matrixMap_sheetNames$items_specific]][, itemLabelCol]));
    stop("Not all specific items have a label! Specifically, item(s) ",
         vecTxtQ(labelLess), " has or have no label. The corresponding ",
         "item identifiers is or are ",
         vecTxtQ(matrixMap[[matrixMap_sheetNames$items_specific]][labelLess, itemIdCol]),
         ".");
  }

  if (type == "simple") {

    if (is.null(contributors)) {
      contributors = "";
    }

    res <- list();
    res[[contributorTable_sheetNames$contributor_table]] <-
      data.frame(
        c(
          contributors,
          rep("", max(0, nContrib - length(contributors)))
        )
      );

    res[[contributorTable_sheetNames$contributor_table]] <-
      cbind(
        res[[contributorTable_sheetNames$contributor_table]],
        as.data.frame(
          rep(
            list(
              rep("", nContrib)
            ),
            times = nrow(matrixMap[[matrixMap_sheetNames$items_specific]])
          )
        )
      );

    names(res[[contributorTable_sheetNames$contributor_table]]) <-
      c(contributorColName,
        matrixMap[[matrixMap_sheetNames$items_specific]][, itemLabelCol]);

    res[[contributorTable_sheetNames$metadata]] <-
      matrixMap[[matrixMap_sheetNames$metadata]];

    res[[contributorTable_sheetNames$item_specs]] <-
      matrixMap[[matrixMap_sheetNames$items_specific]];

    res[[contributorTable_sheetNames$section_specs]] <-
      matrixMap[[matrixMap_sheetNames$sections_specific]];

    res[[matrixMap_sheetNames$item_mapping]] <-
      matrixMap[[matrixMap_sheetNames$item_mapping]];

    res[[matrixMap_sheetNames$items_generic]] <-
      matrixMap[[matrixMap_sheetNames$items_generic]];

    res[[matrixMap_sheetNames$sections_generic]] <-
      matrixMap[[matrixMap_sheetNames$sections_generic]];


    class(res) <- c("comma", "comma_contribTable", "list");


  } else {
    stop("Other types are not yet supported!");
  }

  if (is.null(output)) {
    return(res);
  } else {

    ### Create workbook and add worksheet

    wb <- openxlsx::createWorkbook(
      creator = "comma R package",
      title = contributorTable_sheetNames$contributor_table
    );

    openxlsx::addWorksheet(
      wb,
      sheetName = contributorTable_sheetNames$contributor_table
    );

    ### Add data

    openxlsx::writeData(
      wb,
      sheet = contributorTable_sheetNames$contributor_table,
      res[[contributorTable_sheetNames$contributor_table]],
      rowNames = FALSE
    );

    ### Add styles

    openxlsx::addStyle(
      wb, sheet = contributorTable_sheetNames$contributor_table,
      rows = 1,
      cols = 1:ncol(res[[contributorTable_sheetNames$contributor_table]]),
      gridExpand = TRUE,
      openxlsx::createStyle(
        textDecoration = "bold",
        wrapText = TRUE,
        fgFill = "#ecdbd5"
      )
    );

    ### Style for even rows
    for (i in (2:(nContrib+1))[c(TRUE, FALSE)]) {
      openxlsx::addStyle(
        wb, sheet = contributorTable_sheetNames$contributor_table,
        rows = i,
        cols = 1:ncol(res[[contributorTable_sheetNames$contributor_table]]),
        openxlsx::createStyle(
          fgFill = "#c1e3d7"
        )
      );
    }

    ### Style for odd rows
    for (i in (2:(nContrib+1))[c(FALSE, TRUE)]) {
      openxlsx::addStyle(
        wb, sheet = contributorTable_sheetNames$contributor_table,
        rows = i,
        cols = 1:ncol(res[[contributorTable_sheetNames$contributor_table]]),
        openxlsx::createStyle(
          fgFill = "#eff8f5"
        )
      );
    }

    ### Set widths and freeze top row

    openxlsx::setColWidths(
      wb, sheet = contributorTable_sheetNames$contributor_table,
      cols = 1,
      widths = 20
    );

    openxlsx::setColWidths(
      wb, sheet = contributorTable_sheetNames$contributor_table,
      cols = 2:ncol(res[[contributorTable_sheetNames$contributor_table]]),
      widths = 10
    );

    openxlsx::freezePane(
      wb, sheet = contributorTable_sheetNames$contributor_table,
      firstRow = TRUE,
      firstCol = TRUE
    );

    ### Add descriptions and identifiers as comments
    for (itemNr in 1:nrow(matrixMap$items_specific)) {

      openxlsx::writeComment(
        wb = wb,
        sheet = contributorTable_sheetNames$contributor_table,
        col = 1 + itemNr,
        row = 1,
        comment = openxlsx::createComment(
          comment =
            matrixMap[[matrixMap_sheetNames$items_specific]][itemNr, itemDescrCol],
          author = "comma R package",
          width = 6,
          height = 6,
          visible = FALSE
        )
      );

    }

    ### Add worksheet with metadata

    openxlsx::addWorksheet(
      wb = wb,
      sheetName = contributorTable_sheetNames$metadata
    );

    openxlsx::writeData(
      wb = wb,
      sheet = contributorTable_sheetNames$metadata,
      x = matrixMap[[matrixMap_sheetNames$metadata]],
      rowNames = FALSE
    );

    ### Add worksheet with item specifications

    openxlsx::addWorksheet(
      wb = wb,
      sheetName = contributorTable_sheetNames$item_specs
    );

    openxlsx::writeData(
      wb = wb,
      sheet = contributorTable_sheetNames$item_specs,
      x = matrixMap[[matrixMap_sheetNames$items_specific]],
      rowNames = FALSE
    );

    ### Add worksheet with section specifications

    openxlsx::addWorksheet(
      wb = wb,
      sheetName = contributorTable_sheetNames$section_specs
    );

    openxlsx::writeData(
      wb = wb,
      sheet = contributorTable_sheetNames$section_specs,
      x = matrixMap[[matrixMap_sheetNames$sections_specific]],
      rowNames = FALSE
    );

    ### Optionally, embed matrix map
    if (embed_matrixMap) {

      openxlsx::addWorksheet(
        wb = wb,
        sheetName = matrixMap_sheetNames$item_mapping
      );

      openxlsx::writeData(
        wb = wb,
        sheet = matrixMap_sheetNames$item_mapping,
        x = matrixMap[[matrixMap_sheetNames$item_mapping]],
        rowNames = FALSE
      );

      openxlsx::addWorksheet(
        wb,
        sheetName = matrixMap_sheetNames$items_generic
      );

      openxlsx::writeData(
        wb,
        sheet = matrixMap_sheetNames$items_generic,
        x = matrixMap[[matrixMap_sheetNames$items_generic]],
        rowNames = FALSE
      );

      openxlsx::addWorksheet(
        wb,
        sheetName = matrixMap_sheetNames$sections_generic
      );
      openxlsx::writeData(
        wb,
        sheet = matrixMap_sheetNames$sections_generic,
        x = matrixMap[[matrixMap_sheetNames$sections_generic]],
        rowNames = FALSE
      );

    }

    ### Save the workbook

    openxlsx::saveWorkbook(
      wb = wb,
      file = output,
      overwrite = TRUE
    );

    return(invisible(res));

  }

}
