#' Import a contributors table from a spreadsheet
#'
#' @inheritParams read_spreadsheet
#'
#' @return The contributors table
#' @export
import_contributorsTable <- function(x,
                                     localBackup = NULL,
                                     exportGoogleSheet = TRUE,
                                     xlsxPkg = c("openxlsx", "rw_xl", "XLConnect"),
                                     silent = comma::opts$get("silent")) {

  matrixMap_sheetNames <- comma::opts$get('matrixMap_sheetNames');
  contributorTable_sheetNames <- comma::opts$get('contributorTable_sheetNames');
  matrixMap_colNames <- comma::opts$get('matrixMap_colNames');
  contributorTable_colNames <- comma::opts$get('contributorTable_colNames');
  itemIdCol <- matrixMap_colNames$items_specific$item_specific_id;
  itemLabelCol <- matrixMap_colNames$items_specific$item_specific_label;
  itemDescrCol <- matrixMap_colNames$items_specific$item_specific_description;
  contributorColName <- comma::opts$get('contributorTable_contributorColName');

  res <-
    comma::read_spreadsheet(
      x = x,
      localBackup = localBackup,
      exportGoogleSheet = exportGoogleSheet,
      xlsxPkg = xlsxPkg,
      silent = silent
    );

  if (!all(contributorTable_sheetNames %in% names(res))) {
    stop("Not all worksheet names specified in the options were present in ",
         "the file you just imported ('", x, "'). This file contained ",
         "worksheets ", vecTxtQ(names(res)), ", but I need ",
         vecTxtQ(contributorTable_sheetNames),
         ". You can check these names by checking\n\n",
         "    comma::opts$get('contributorTable_sheetNames');\n\n",
         "You can change these names by setting a new value with\n\n",
         "    comma::opts$set(contributorTable_sheetNames = ...);\n\n",
         "(you need to replace the ellipsis (...) with the new names).");
  }

  for (sheetToCheck in names(contributorTable_colNames)) {
    if (!all(contributorTable_colNames[[sheetToCheck]] %in% names(res[[contributorTable_sheetNames[[sheetToCheck]]]]))) {
      stop("In worksheet '", sheetToCheck, "', not all required columns are ",
           "present in the file you just imported ('", x, "'). The worksheet ",
           "contained columns ", vecTxt(names(res[[contributorTable_sheetNames[[sheetToCheck]]]])),
           ", but I need ", vecTxtQ(contributorTable_colNames[[sheetToCheck]]),
           ". You can check these names by checking\n\n",
           "    comma::opts$get('contributorTable_colNames');\n\n",
           "You can change these names by setting a new value with\n\n",
           "    comma::opts$set(contributorTable_colNames = ...);\n\n",
           "(you need to replace the ellipsis (...) with the new names).");
    }
  }

  names(res[[contributorTable_sheetNames$contributor_table]]) <-
    c(contributorColName,
      res$item_specs[, itemIdCol]);

  res[[contributorTable_sheetNames$contributor_table]][, res$item_specs[, itemIdCol]] <-
    lapply(
      res[[contributorTable_sheetNames$contributor_table]][, res$item_specs[, itemIdCol]],
      function(x) {
        return(
          ifelse(
            is.na(x) | (trimws(as.character(x)) == "FALSE") | (nchar(trimws(x)) == 0) |
              (trimws(as.character(x)) == "0"),
            FALSE,
            TRUE
          )
        );
      }
    );

  res[[contributorTable_sheetNames$contributor_table]] <-
    res[[contributorTable_sheetNames$contributor_table]][
      nchar(
        trimws(
          res[[contributorTable_sheetNames$contributor_table]][
            ,
            contributorColName
          ]
        )
      ) > 0,

    ];

  class(res) <- c("comma", "comma_contribTable", "list");

  return(invisible(res));

}
