#' Show how a matrix map maps contributions
#'
#' @param x The matrix map
#' @param mapping The mapping
#' @param output Format of output; either "`console`", "`markdown`", or
#' "`html`".
#' @param specific_to_generic Whether to map from specific to generic (`TRUE`)
#' or from generic to specific (`FALSE`)
#'
#' @return
#' @export
#'
#' @examples ### Initialize an example matrix map
#' initialized_genSysRev_v1 <-
#'   comma::matrixMap_load(
#'     "genSysRev_v1"
#'   );
#'
#' ### Show the contributions
#' comma::show_contribution_mapping(
#'   initialized_genSysRev_v1
#' );
show_contribution_mapping <- function(x,
                                      mapping = NULL,
                                      output = "console",
                                      specific_to_generic = TRUE) {

  matrixMap_sheetNames <- comma::opts$get('matrixMap_sheetNames');
  contributorTable_sheetNames <- comma::opts$get('contributorTable_sheetNames');
  matrixMap_colNames <- comma::opts$get('matrixMap_colNames');
  itemLabelCol <- matrixMap_colNames$items_specific$item_specific_label;
  itemDescrCol <- matrixMap_colNames$items_specific$item_specific_description;

  contributorColName <- comma::opts$get('contributorTable_contributorColName');

  if (output == "console") {
    func <- function(id1, label1, descr1, id2, label2, descr2) {
      return(
        paste0("\n", label1, ": ", descr1, " [", id1, "]\n\n",
               "  -- MAPS ONTO -->\n",
               "\n", label2, ": ", descr2, " [", id2, "]")
      );
    }
  } else if (output == "markdown") {
    func <- function(id1, label1, descr1, id2, label2, descr2) {
      return(
        paste0("\n**", label1, "**: ", descr1, " `[", id1, "]`\n\n",
               "  -- MAPS ONTO -->\n",
               "\n**", label2, "**: ", descr2, " `[", id2, "]`")
      );
    }
  } else if (output == "html") {
    func <- function(id1, label1, descr1, id2, label2, descr2) {
      return(
        paste0("<div><strong>", label1, "</strong>: ", descr1, " <pre>[", id1, "]</pre></div>",
               "<div>  -- MAPS ONTO --></div>",
               "<div><strong>", label2, "</strong>: ", descr2, " <pre>[", id2, "]</pre>")
      );
    }
  }

  if ((!inherits(x, "comma_contribTable")) &&
      (!inherits(x, "comma_mappedContribTable")) &&
      (!inherits(x, "comma_matrixMap"))) {
    stop("As argument `x`, you have to pass a {comma} contributors table ",
         "as imported with ",
         "`comma::import_contributorsTable()` or as created with ",
         "`comma::map_contributions()`. However, you passed an object ",
         "of class ", vecTxtQ(class(x)), ".");
  }

  mapping_specific_to_generic <-
    stats::setNames(x$item_mapping$item_generic_id,
                    nm = x$item_mapping$item_specific_id);

  mapping_generic_to_specific <-
    stats::setNames(x$item_mapping$item_specific_id,
                    nm = x$item_mapping$item_generic_id);

  labels_specific <-
    stats::setNames(x$items_specific$item_specific_label,
                    x$items_specific$item_specific_id);

  descriptions_specific <-
    stats::setNames(x$items_specific$item_specific_description,
                    x$items_specific$item_specific_id);

  labels_generic <-
    stats::setNames(x$items_generic$item_generic_label,
                    x$items_generic$item_generic_id);

  descriptions_generic <-
    stats::setNames(x$items_generic$item_generic_description,
                    x$items_generic$item_generic_id);

  res <- c();

  if (specific_to_generic) {

    for (id in names(mapping_specific_to_generic)) {
      res <- c(res,
               func(id1 = id,
                    label1 = labels_specific[id],
                    descr1 = descriptions_specific[id],
                    id2 = mapping_specific_to_generic[id],
                    label2 = labels_generic[mapping_specific_to_generic[id]],
                    descr2 = descriptions_generic[mapping_specific_to_generic[id]]));
    }

  } else {

    for (id in names(mapping_generic_to_specific)) {
      res <- c(res,
               func(id1 = id,
                    label1 = labels_generic[id],
                    descr1 = descriptions_generic[id],
                    id2 = mapping_generic_to_specific[id],
                    label2 = labels_specific[mapping_generic_to_specific[id]],
                    descr2 = descriptions_specific[mapping_generic_to_specific[id]]));
    }

  }

  cat(res, sep="\n\n---------------------\n");

  return(invisible(res));

}
