
<!-- README.md is generated from README.Rmd. Please edit that file -->

# <img src='man/figures/logo.png' align="right" height="138" /> comma 📦

## Contribution Matrix Mapping

<!-- badges: start -->

[![CRAN
status](https://www.r-pkg.org/badges/version/comma)](https://cran.r-project.org/package=comma)

[![Dependency
status](https://tinyverse.netlify.com/badge/comma)](https://CRAN.R-project.org/package=comma)

[![Pipeline
status](https://gitlab.com/r-packages/comma/badges/main/pipeline.svg)](https://gitlab.com/r-packages/comma/-/commits/main)

[![Downloads last
month](https://cranlogs.r-pkg.org/badges/last-month/comma?color=brightgreen)](https://cran.r-project.org/package=comma)

[![Total
downloads](https://cranlogs.r-pkg.org/badges/grand-total/comma?color=brightgreen)](https://cran.r-project.org/package=comma)

[![Coverage
status](https://codecov.io/gl/r-packages/comma/branch/main/graph/badge.svg)](https://app.codecov.io/gl/r-packages/comma?branch=main)

<!-- badges: end -->

The pkgdown website for this project is located at
<https://r-packages.gitlab.io/comma>.

<!--- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package  -->
<!--- - - - - - - - - - - - - - - - - - - - - -->

To streamline and standardize the reporting of author contributions in
academic publications, the Contributor Roles Taxonomy (CRediT) is a
valuable tool. It is a high-level taxonomy, not specific to any academic
discipline, and therefore lends itself well to widespread adoption and
implementation.

However, this generic nature has a flip side: how specific tasks map
onto its categories isn’t always clear. This means that publications
reporting on two very similar studies may nonetheless map their
constituent tasks onto CRediT roles differently.

`comma` offers a solution to this by providing means to map contribution
matrices onto each other. This enables specification of contribution
matrix mappings for specific study types within a discipline, further
CRediT’s aim of transparently communicating each contributor’s specific
contribution.

However, while on the one hand `comma` can help to calibrate study
contributions so they map onto CRediT consistently, on the other hand,
`comma` also helps mapping CRediT onto other taxonomies. Already, the
Contribution Role Ontology has been developed that extends CRediT; in
the future, alternative taxonomies will likely surface, too.

<!--- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package   -->
<!--- - - - - - - - - - - - - - - - - - - - - -->

## Installation

You can install the released version of `comma` from
[CRAN](https://CRAN.R-project.org) with:

``` r
install.packages('comma');
```

You can install the development version of `comma` from
[GitLab](https://about.gitlab.com) with:

``` r
remotes::install_gitlab('r-packages/comma');
```

(assuming you have `remotes` installed; otherwise, install that first
using the `install.packages` function)

You can install the cutting edge development version (own risk, don’t
try this at home, etc) of `comma` from
[GitLab](https://about.gitlab.com) with:

``` r
remotes::install_gitlab('r-packages/comma@dev');
```

<!--- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package  -->
<!--- - - - - - - - - - - - - - - - - - - - - -->
<!--- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package   -->
<!--- - - - - - - - - - - - - - - - - - - - - -->
