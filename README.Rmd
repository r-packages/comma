---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r setup, include=FALSE}

knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)

# install.packages('pkgnet');

packagename <- 'comma';
packageSubtitle <- "Contribution Matrix Mapping";

gitLab_ci_badge <-
  paste0("https://gitlab.com/r-packages/", packagename, "/badges/main/pipeline.svg");
gitLab_ci_url <-
  paste0("https://gitlab.com/r-packages/", packagename, "/-/commits/main");

codecov_badge <-
  paste0("https://codecov.io/gl/r-packages/", packagename, "/branch/main/graph/badge.svg");
codecov_url <-
  paste0("https://app.codecov.io/gl/r-packages/", packagename, "?branch=main");

dependency_badge <-
  paste0("https://tinyverse.netlify.com/badge/", packagename);
dependency_url <-
  paste0("https://CRAN.R-project.org/package=", packagename);

cran_badge <-
  paste0("https://www.r-pkg.org/badges/version/", packagename);
cran_url <-
  paste0("https://CRAN.R-project.org/package=", packagename);
cran_url <-
  paste0("https://cran.r-project.org/package=", packagename);
cranVersion_badge <-
  paste0("https://www.r-pkg.org/badges/version/", packagename, "?color=brightgreen");
cranLastMonth_badge <-
  paste0("https://cranlogs.r-pkg.org/badges/last-month/", packagename, "?color=brightgreen");
cranTotal_badge <-
  paste0("https://cranlogs.r-pkg.org/badges/grand-total/", packagename, "?color=brightgreen");

pkgdown_url <-
  paste0("https://r-packages.gitlab.io/", packagename);

```

# <img src='man/figures/logo.png' align="right" height="138" /> `r paste(packagename, "\U1F4E6")`

## `r packageSubtitle`

<!-- badges: start -->

[![CRAN status](`r cran_badge`)](`r cran_url`)

[![Dependency status](`r dependency_badge`)](`r dependency_url`)

[![Pipeline status](`r gitLab_ci_badge`)](`r gitLab_ci_url`)

[![Downloads last month](`r cranLastMonth_badge`)](`r cran_url`)

[![Total downloads](`r cranTotal_badge`)](`r cran_url`)

[![Coverage status](`r codecov_badge`)](`r codecov_url`)

<!-- badges: end -->

The pkgdown website for this project is located at `r pkgdown_url`.

<!--- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package  -->
<!--- - - - - - - - - - - - - - - - - - - - - -->

To streamline and standardize the reporting of author contributions in academic publications, the Contributor Roles Taxonomy (CRediT) is a valuable tool. It is a high-level taxonomy, not specific to any academic discipline, and therefore lends itself well to widespread adoption and implementation.

However, this generic nature has a flip side: how specific tasks map onto its categories isn't always clear. This means that publications reporting on two very similar studies may nonetheless map their constituent tasks onto CRediT roles differently.

`comma` offers a solution to this by providing means to map contribution matrices onto each other. This enables specification of contribution matrix mappings for specific study types within a discipline, further CRediT's aim of transparently communicating each contributor's specific contribution.

However, while on the one hand `comma` can help to calibrate study contributions so they map onto CRediT consistently, on the other hand, `comma` also helps mapping CRediT onto other taxonomies. Already, the Contribution Role Ontology has been developed that extends CRediT; in the future, alternative taxonomies will likely surface, too.

<!--- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package   -->
<!--- - - - - - - - - - - - - - - - - - - - - -->

## Installation

You can install the released version of ``r packagename`` from [CRAN](https://CRAN.R-project.org) with:

```{r echo=FALSE, comment="", results="asis"}
cat(paste0("``` r
install.packages('", packagename, "');
```"));
```

You can install the development version of ``r packagename`` from [GitLab](https://about.gitlab.com) with:

```{r echo=FALSE, comment="", results="asis"}
cat(paste0("``` r
remotes::install_gitlab('r-packages/", packagename, "');
```"));
```

(assuming you have `remotes` installed; otherwise, install that first using the `install.packages` function)

You can install the cutting edge development version (own risk, don't try this at home, etc)  of ``r packagename`` from [GitLab](https://about.gitlab.com) with:

```{r echo=FALSE, comment="", results="asis"}
cat(paste0("``` r
remotes::install_gitlab('r-packages/", packagename, "@dev');
```"));
```

<!--- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package  -->
<!--- - - - - - - - - - - - - - - - - - - - - -->


<!--- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package   -->
<!--- - - - - - - - - - - - - - - - - - - - - -->
